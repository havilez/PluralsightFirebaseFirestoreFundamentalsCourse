var db = firebase.firestore();
const settings = { timestampsInSnapshots: true };
db.settings(settings);

// create reference for collection
var employeesRef = db.collection("employees");

employeesRef.get().then(querySnapshot => {
  querySnapshot.forEach(doc => {
    console.log(`${doc.id}`);
  });
});

// Only do his ONCE!!!!!
// Add data to employees collection

// employeesRef.doc("R.Dikles").set({
//     fname: "Ranice",
//     lname: "Dikles",
//     email: "rdikles0@hatena.ne.jp",
//     age: 39,
//     gender: 'Female',
//     yearsOfExperience: 9,
//     isFullTime: true,

// });

// employeesRef.doc("J.Allen").set({
//     fname: "Jarrett",
//     lname: "Allen",
//     email: "jallen1@hatena.ne.jp",
//     age: 20,
//     gender: "Male",
//     yearsOfExperience: 1,
//     isFullTime: true
// });

// employeesRef.doc("D.Carroll").set({
//     fname: "Demarre",
//     lname: "Carroll",
//     email: "dcarroll2@hatena.ne.jp",
//     gender: 'Male',
//     yearsOfExperience: 9,
//     isFullTime: true
// })

// FIX-ME: Not NEEDED !!!
// employeesRef.get().then(function(querySnapshot) {
//     var tableRow='';
//     querySnapshot.forEach(function(doc) {
//         var document = doc.data();
//         tableRow +='<tr>';
//         tableRow += '<td class="fname">' + document.fname + '</td>';
//         tableRow += '<td class="lname">' + document.lname + '</td>';
//         tableRow += '<td class="email">' + document.email + '</td>';
//         tableRow += '<td class="age">' + document.age + '</td>';
//         tableRow += '<td class="gender">' + document.gender + '</td>';
//         tableRow += '<td class="yearsofexperience">' + document.yearsOfExperience + '</td>';
//         tableRow += '<td class="isfulltime">' + document.isFullTime + '</td>';
//         tableRow += '<td class="editEmployee"><i class="fa fa-pencil" aria-hidden="true" style="color:green"></i></td>'
//         tableRow += '<td class="deleteEmployee"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></td>'
//         tableRow += '</tr>';
//     });
//     $('tbody.tbodyData').append(tableRow);
// });