$(document).ready(function() {
            $('#onlyMalesFilter').click(function(){
                // console.log('onlyMalesFilter Filter executed');
                // syntax: collRef.where("property","operation","value")
                employeesRef.where("gender","==","Male")
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    })

            });

            $('#fullTimeFilter').click(function(){
                // console.log('fullTimeFilter Filter executed');
                employeesRef.where("isFullTime","==",true)
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    });
            });

            $('#olderThenFilter').click(function(){
                // console.log('olderThenFilter Filter executed');
                employeesRef.where("age",">=",30)
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    });
            });

            $('#ageBetweenFilter').click(function(){
                // console.log('ageBetweenFilter Filter executed');
                employeesRef.where("age", ">", 35 ).where("age", "<", 50 )
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    });

            });

            $('#yearsOfExperienceFilter').click(function(){
                // console.log('yearsOfExperienceFilter Filter executed');
                employeesRef.where("gender", "==", "Female")
                employeesRef.where("yearsOfExperience", ">=", 5 ).where("yearsOfExperience", "<=", 10 )
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    });

            });

            $('#clearFilter').click(function(){
                // console.log('clearFilter Filter executed');
                employeesRef.orderBy("fname","desc").get().then(( function (querySnapshot) {
                    LoadTableData(querySnapshot);
                }))

            });


            $("#searchEmployee" ).change(function() {
                // console.log('You entered: ', $(this).val());
                //TODO: change search filtering to work as user types in each letter in field
                var searchValue = $(this).val();
                employeesRef.where("fname", "==", searchValue )
                    .onSnapshot( function (querySnapshot) {
                        LoadTableData(querySnapshot);
                    });

            });
});

db.collection("employees").onSnapshot( function (snapShot) {
    console.log("Something changed");
    snapShot.docChanges().forEach( function ( change ) {
        console.log("Change =" ,change);

        if ( change.type === "added") {
            console.log("Employee Added ");
        }
        if ( change.type === "modified ") {
            console.log( "Employee Modified ");
        }
        if ( change.type === "removed") {
            console.log("Employee Removed ");
        }

    });


    LoadTableData( snapShot);

});

// map employee data into html table row , add table row to html table body
function LoadTableData(querySnapshot){
    var tableRow='';
    // access a document from employees collection in Firestore
    // store in table row in html table, then add html table row to html table body

    querySnapshot.forEach(function(doc) {

        // for each document create a dom node for a html table row
        // map employee document fields into corresponding table data column within table row
        var document = doc.data();
        tableRow +='<tr>';
        tableRow += '<td class="fname">' + document.fname + '</td>';
        tableRow += '<td class="lname">' + document.lname + '</td>';
        tableRow += '<td class="email">' + document.email + '</td>';
        tableRow += '<td class="age">' + document.age + '</td>';
        tableRow += '<td class="gender">' + document.gender + '</td>';
        tableRow += '<td class="yearsofexperience">' + document.yearsOfExperience + '</td>';
        tableRow += '<td class="isfulltime">' + document.isFullTime + '</td>';
        tableRow += '<td class="editEmployee"><i class="fa fa-pencil" aria-hidden="true" style="color:green"></i></td>'
        tableRow += '<td class="deleteEmployee"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></td>'
        tableRow += '</tr>';
    });

    // insert  html table row inside html table body
    $('tbody.tbodyData').html(tableRow);
}